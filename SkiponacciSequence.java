public class SkiponacciSequence extends Sequence {

    private int number1;
    private int number2;
    private int number3;

    public SkiponacciSequence(int number1, int number2, int number3) {
        this.number1 = number1;
        this.number2 = number2;
        this.number3 = number3;
    }

    public int getTerm(int n) {
        if (n == 0) {
            return this.number1;
        } else if (n == 1) {
            return this.number2;
        } else if (n == 2) {
            return this.number3;
        } else {
            int secondToLast = this.number1;
            int last = this.number2;
            int current = this.number3;
            for (int i = 3; i <= n; i++) {
                int newCurrent = last + secondToLast;
                secondToLast = last;
                last = current;
                current = newCurrent;
            }
            return current;
        }
    }
}