import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;

public class TestFibonacciSequence {

    @Test
    void TestGetFisrt() {
        FibonacciSequence seq = new FibonacciSequence(2, 4);
        assertEquals(2, seq.getTerm(0));
    }

    @Test
    void TestGetSecond() {
        FibonacciSequence seq = new FibonacciSequence(2, 4);
        assertEquals(4, seq.getTerm(1));
    }

    @Test
    void TestGetNinth() {
        FibonacciSequence seq = new FibonacciSequence(2, 4);
        assertEquals(110, seq.getTerm(8));
    }

    @Test
    void testTermTwice() {
        FibonacciSequence seq = new FibonacciSequence(2, 4);
        assertEquals(6, seq.getTerm(2));
        assertEquals(6, seq.getTerm(2));
    }
}
