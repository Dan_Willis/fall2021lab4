import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;

public class TestSkiponacciSequence {

    @Test
    public void testGetFirst() {
        SkiponacciSequence seq = new SkiponacciSequence(2, 4, 1);
        assertEquals(2, seq.getTerm(0));
    }

    @Test
    public void testGetSecond() {
        SkiponacciSequence seq = new SkiponacciSequence(2, 4, 1);
        assertEquals(4, seq.getTerm(1));
    }

    @Test
    public void testGetThird() {
        SkiponacciSequence seq = new SkiponacciSequence(2, 4, 1);
        assertEquals(1, seq.getTerm(2));
    }

    @Test
    public void testGetNinth() {
        SkiponacciSequence seq = new SkiponacciSequence(2, 4, 1);
        assertEquals(18, seq.getTerm(8));
    }

    @Test
    public void testGetTermTwice() {
        SkiponacciSequence seq = new SkiponacciSequence(2, 4, 1);
        assertEquals(18, seq.getTerm(8));
        assertEquals(18, seq.getTerm(8));
    }

}
