public class FibonacciSequence extends Sequence {

    private int number1;
    private int number2;

    public FibonacciSequence(int number1, int number2) {
        this.number1 = number1;
        this.number2 = number2;
    }

    public int getTerm(int n) {
        if (n == 0) {
            return this.number1;
        } else if (n == 1) {
            return this.number2;
        }

        int last = this.number1;
        int current = this.number2;
        for (int i = 2; i <= n; i++) {
            int newCurrent = current + last;
            last = current;
            current = newCurrent;
        }
        return current;

    }
}
