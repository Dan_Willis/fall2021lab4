import java.util.Scanner;

public class Application {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Enter info for sequences:");
        String info = input.next();
        input.close();

        Sequence[] sequences;
        sequences = parse(info);
        print(sequences, 10);

    }

    public static void print(Sequence[] seq, int n) {

        for (int i = 0; i < seq.length; i++) {
            System.out.println("\n");
            for (int j = 0; j < n; j++) {
                System.out.print(seq[i].getTerm(j) + ",");
            }
        }
    }

    public static Sequence[] parse(String info) {
        String[] seqInfo = info.split(";");
        Sequence[] sequences = new Sequence[seqInfo.length / 4];

        for (int i = 0; i < sequences.length; i++) {
            if (seqInfo[i * 4].equals("Fib")) {
                int number1 = Integer.parseInt(seqInfo[(i * 4) + 1]);
                int number2 = Integer.parseInt(seqInfo[(i * 4) + 2]);
                sequences[i] = new FibonacciSequence(number1, number2);
            } else if (seqInfo[i * 4].equals("Skip")) {
                int number1 = Integer.parseInt(seqInfo[(i * 4) + 1]);
                int number2 = Integer.parseInt(seqInfo[(i * 4) + 2]);
                int number3 = Integer.parseInt(seqInfo[(i * 4) + 3]);
                sequences[i] = new SkiponacciSequence(number1, number2, number3);
            }
        }
        return sequences;
    }
}
